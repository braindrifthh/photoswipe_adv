# Drupal 7 module

This module makes some changes to the [PhotoSwipe](https://www.drupal.org/project/photoswipe) module.

### Remove PHP Notices
This notices are generated when a default image is shown by an image field:    
`Notice: Undefined index: height in theme_photoswipe_image_formatter() (line 86 of .../photoswipe/photoswipe.theme.inc).`  
`Notice: Undefined index: width in theme_photoswipe_image_formatter() (line 86 of .../photoswipe/photoswipe.theme.inc).`  
